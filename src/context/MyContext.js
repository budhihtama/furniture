// src/context/MyContext.js
import React, { createContext, useState } from "react";

const MyContext = createContext(null);

export const MyProvider = ({ children }) => {
  const [state, setState] = useState("Hello, World!");

  return (
    <MyContext.Provider value={{ state, setState }}>
      {children}
    </MyContext.Provider>
  );
};

export default MyContext;
