// src/components/Products.js
import React from 'react';
import Slider from 'react-slick';
import sofa from '../assets/images/sofa.jpg';
import chair from '../assets/images/chair.jpg';
import table from '../assets/images/table.jpg';
import '../styles/Product.css';

const products = [
  { id: 1, name: 'Sofa', price: '$499', image: sofa },
  { id: 2, name: 'Chair', price: '$199', image: chair },
  { id: 3, name: 'Table', price: '$299', image: table },
];

const Products = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      }
    ]
  };

  return (
    <section id="products" className="products">
      <div className="container">
        <h2>Our Products</h2>
        <Slider {...settings} className="product-list">
          {products.map(product => (
            <div key={product.id} className="product">
              <img src={product.image} alt={product.name} />
              <h3>{product.name}</h3>
              <p>{product.price}</p>
            </div>
          ))}
        </Slider>
      </div>
    </section>
  );
};

export default Products;
