// src/components/Header.js
import React from 'react';
// import './Header.css';

const Header = () => {
  return (
    <header className="header">
      <div className="container">
        <h1>Ashiappp Store</h1>
        <nav>
          <ul>
            <li><a href="#hero">Home</a></li>
            <li><a href="#products">Products</a></li>
            <li><a href="#footer">Contact</a></li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
