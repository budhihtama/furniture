// src/components/Hero.js
import React from 'react';

const Hero = () => {
  return (
    <section id="hero" className="hero">
      <div className="container">
        <h2>Welcome to Our Furniture Store</h2>
        <p>Discover the best furniture for your home.</p>
        <button>Shop Now</button>
      </div>
    </section>
  );
};

export default Hero;
