// src/components/Footer.js
import React from "react";

const Footer = () => {
  return (
    <footer id="footer" className="footer">
      <div className="container">
        <p>&copy; 2024 Furniture Store. All Rights Reserved.</p>
        <p>Contact us: email@example.com</p>
      </div>
    </footer>
  );
};

export default Footer;
