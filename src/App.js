// src/App.js
import React from "react";
import "./styles/App.css" 
import Footer from "./components/Footer";
import Header from "./components/Header";
import Hero from "./components/Hero";
import Products from "./components/Products";

function App() {
  return (
    <div className="App">
      <Header />
      <Hero />
      <Products />
      <Footer />
    </div>
  );
}

export default App;
